defmodule MyAppWeb.Router do
  use MyAppWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MyAppWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/users", UserController, only: [:new, :create]

  end

  # Other scopes may use custom stacks.
  # scope "/api", MyAppWeb do
  #   pipe_through :api
  # end
    scope "/api", MyAppWeb.Api, as: :api do
     pipe_through :api
    resources "/users", UserController, only: [:index, :show]
    end
end


# http://localhost:4000/users/new
# http://localhost:4000/api/users
# http://localhost:4000/api/users/3