defmodule   MyAppWeb.Api.UserView do
  use MyAppWeb, :view

  def render("index.json", %{users: users}) do
    %{data: render_many(users, MyAppWeb.Api.UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{firstname: user.firstname,
      dob: user.dob
   }
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, MyAppWeb.Api.UserView, "user.json")}
  end

end
