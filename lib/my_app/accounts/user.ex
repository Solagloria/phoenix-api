defmodule MyApp.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :dob, :date
    field :firstname, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:firstname, :dob])
    |> validate_required([:firstname, :dob])
  end
end
